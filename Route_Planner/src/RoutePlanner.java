import java.io.*;

public class RoutePlanner {
	
	//str is the string array that stores data which is read from file
	public static String[] str=new String[6];
	//sub is 2D array that stores data of str which is separated by comma in the form of array
	public static String[][] sub=new String[6][5];
	
	
  //User defined method which gives length of array
  public int Length(String arr[])
  {
	  int l=0;
	  for(int i=0;arr[i]!="end";i++)
		  l++;
	  return l;
  }
  
 
  
 //method that reads from written file and stores data in array str
  
    public static void dataFile() throws Exception
  {
	  FileWriter f=new FileWriter("src/routes.csv");
      String header="From,To,Distance,Travel Time,Airfare\nDelhi,Mumbai,1148,2:10,INR 6000\nDelhi,London,6704,8:55,INR 37000\nDelhi,Frankfurt,6117,8:35,INR 32000\nLosAngeles,Tokyo,8773,11:15,USD 520\nFrankfurt,London,660,1:25,EUR 155";
      
      f.write(header);
      f.flush();
      f.close();
      FileReader fr=new FileReader("src/routes.csv");
      BufferedReader br=new BufferedReader(fr);
      for(int i=0;i<6;i++)
      	str[i]=br.readLine();
      br.close();
      for(int i=0;i<6;i++){
      		sub[i]=str[i].split(",");
      }
     
	  
  }
    //Displays data in arrays as a table
  public static void disp(String[][] sub){
	  
      for(int i=0;i<6;i++)
      	System.out.format("%20s %20s %20s %20s %20s",sub[i][0],sub[i][1],sub[i][2],sub[i][3],sub[i][4]+"\n");
     System.out.println("\n");
	}
  
  //Shows the direct connecting flights from the desired city
  public void showDirectFlights(String[] routeInfo,String fromCity)
  {
	  
	  String[] dirflights= DirectFlights(routeInfo,fromCity);
	  int x=Length(dirflights);
	 if(x==0){
		 System.out.println("We are sorry,At this point of time we do not have any information on flights originating from " + fromCity);
	      System.out.println("\n");}
	 
    else
    {
	  String subdirflights[][]=new String[3][5];
	  for(int i=0; i<x; i++)
		  subdirflights[i]=dirflights[i].split(",");
	  
	  System.out.format("%20s %20s %20s %20s %20s",sub[0][0],sub[0][1],sub[0][2],sub[0][3],sub[0][4]+"\n");
	  for(int i=0;i<x;i++)		  
	    		System.out.format("%20s %20s %20s %20s %20s",subdirflights[i][0],subdirflights[i][1],subdirflights[i][2],subdirflights[i][3],subdirflights[i][4]+"\n");
	 
	  System.out.println("\n");
  
   }	 
  
  }
  
  //Finds the direct flights available from a desired city
public String[] DirectFlights(String[] routeInfo,String fromCity){
		 
 int l=0;
 String[] dirflights=new String[10];

 for(int i=1;i<6;i++){
	  if(sub[i][0].equalsIgnoreCase(fromCity)){ 
			  dirflights[l]=routeInfo[i];
			  l++;
	}
}
 dirflights[l]="end";  
 return dirflights;  
		   
	
}	  
		 
//shows the data of direct flights in sorted manner 
public  void sortDirectFlights(String[] directflights)
{
	  int x=Length(directflights);
	  if(x!=0){
	  String subdirflights[][]=new String[3][5];
	  for(int i=0; i<x; i++)
		  subdirflights[i]=directflights[i].split(",");
	
	 String temp;
  
    for(int i=0; i<x; i++)
    {
  	  for(int j=i+1;j<x; j++)
  	  {
  	  if(subdirflights[i][1].compareTo(subdirflights[j][1])>0)
  	  {
  		 temp=directflights[i];
  		 directflights[i]=directflights[j];
  		 directflights[j]=temp;
  	  }
  	  }
    }
    for(int i=0; i<x; i++)
		  subdirflights[i]=directflights[i].split(",");
    System.out.format("%20s %20s %20s %20s %20s",sub[0][0],sub[0][1],sub[0][2],sub[0][3],sub[0][4]+"\n");
    
    for(int i=0; i<x; i++)
  	  System.out.format("%20s %20s %20s %20s %20s",subdirflights[i][0],subdirflights[i][1],subdirflights[i][2],subdirflights[i][3],subdirflights[i][4]+"\n");
		
	  }
	  System.out.println("\n");
}
  
//shows all the possible routes between 2 cities	  
public void showAllConnections(String[] routeInfo,String fromCity,String toCity){
	  String destination=toCity;
	  String[] s=Flights(routeInfo,fromCity,toCity,destination);
	 
	  int p=Length(s);
	  String subflights[][]=new String[20][5];
		
		 for(int i=0; i<p; i++)
			  subflights[i]=s[i].split(",");
		 System.out.format("%20s %20s %20s %20s %20s",sub[0][0],sub[0][1],sub[0][2],sub[0][3],sub[0][4]+"\n");
		 for(int i=0; i<p; i++) {
			 System.out.format("%20s %20s %20s %20s %20s",subflights[i][0],subflights[i][1],subflights[i][2],subflights[i][3],subflights[i][4]+"\n");
			
		 }
		 System.out.println("\n"); 
		   
}

//finds all the possible routes between 2 cities
public String[] Flights(String routeInfo[],String fromCity,String toCity,String destination)
{
	  int l=0;
	  String[] s=new String[20];
	  String[] s1=new String[20];
	  String[] connections=new String[100];
	  
	  int y=0;
	  y=Length(DirectFlights(routeInfo,fromCity));
	  String[] dirflights=DirectFlights(routeInfo,fromCity);
	  String subdirflights[][]=new  String[3][5];
	  
	  for(int i=0; i<y; i++)
		  subdirflights[i]=dirflights[i].split(",");
	  
	  for(int h=0; h<y; h++) {
		  int flag=0,n=0;
	      toCity=subdirflights[h][1];
	     
	      do{  
	    	  if( !(toCity.equalsIgnoreCase(sub[1][0]))&&!(toCity.equalsIgnoreCase(sub[2][0]))&&!(toCity.equalsIgnoreCase(sub[3][0]))&&!(toCity.equalsIgnoreCase(sub[4][0]))&&!(toCity.equalsIgnoreCase(sub[5][0])) && !(toCity.equalsIgnoreCase(destination))){
	    		  flag=-1; 
	    		  break;
	    		  }
	    	 
	    	  for(int i=0;i<routeInfo.length;i++){
	    		  
	    		  if(sub[i][0].equalsIgnoreCase(fromCity)&&sub[i][1].equalsIgnoreCase(toCity))
	    		  {
	    			  s1[n]=routeInfo[i];
	    			  n++;
	    			  if(!(sub[i][1].equalsIgnoreCase(destination))){
	    				  fromCity=sub[i][1];
	    				  toCity=destination;
	    				  Flights(routeInfo,fromCity,toCity,destination); 
	    			  }
	    			 
	    		  }
	    			  
	    	  }
	    	  }while(!(destination.equalsIgnoreCase(toCity)));
	      
	      if (flag==-1){
			  s[0]="end";
			  
		  }
		  else
		  {
			 int i;
			for(i=0;i<n;i++)
				s[i]=s1[i];
			s[i]="end";
		  }
	      for( int j=0;j<Length(s);j++){
				 connections[l]=s[j];
				 l++;
	      }
	      
	    	
	  }
	  connections[l]="end";
	 
	  return connections;
}

	public static void main(String[] args) throws Exception
	{
		
       dataFile();
       disp(sub);
       RoutePlanner r=new RoutePlanner ();
       r.showDirectFlights(str,"Delhi");
       r.sortDirectFlights(r.DirectFlights(str,"Delhi"));
       r.showAllConnections(str,"Delhi","London");

}
}
